;;; /home/wzn/.emacs.d/lisp/init-basic.el  -*- lexical-binding: t; -*- 

(defcustom +transparency 90
  "Transparency value effect my own transparency settings"
  :type 'integer)

;;; Behavior settings
;; disable startup screen
(setq inhibit-startup-screen t)

;; yes-and-no to y-and-n
(defalias 'yes-or-no-p 'y-or-n-p)

;; disable anoising visual/(or not) bell
(setq ring-bell-function 'ignore)

;; disable annoying cursor blink
(blink-cursor-mode -1)

;; Only start server when server not running
(require 'server)
(when (not (server-running-p))
  (server-start))

;; let emacs scrool smoothly
(setq redisplay-dont-pause t
      scroll-margin 1
      scroll-step 1
      scroll-conservatively 10000
      scroll-preserve-screen-position 1)

;;; Scrolling.
;; Good speed and allow scrolling through large images (pixel-scroll).
;; Note: Scroll lags when point must be moved but increasing the number
;;       of lines that point moves in pixel-scroll.el ruins large image
;;       scrolling. So unfortunately I think we'll just have to live with
;;       this.
(pixel-scroll-mode)
(setq pixel-dead-time 0) ; Never go back to the old scrolling behaviour.
(setq pixel-resolution-fine-flag t) ; Scroll by number of pixels instead of lines (t = frame-char-height pixels).
(setq mouse-wheel-scroll-amount '(1)) ; Distance in pixel-resolution to scroll each mouse wheel event.
(setq mouse-wheel-progressive-speed nil) ; Progressive speed is too fast for me.

;; Word wrap by catagory
(setq word-wrap-by-category t)

;; undo-redo
(global-set-key (kbd "C-?") #'undo-redo)
(global-set-key (kbd "C-/") #'undo-only)
(use-package undo-tree
  :config
  (global-undo-tree-mode))

;; ace-window default
;; (global-set-key (kbd "C-x o") #'ace-window)

;; Better default settings
(require 'better-defaults)

;;; Basice Ui settings
;; disable scroll-bar
(scroll-bar-mode -1)

;; doom modeline
(doom-modeline-mode +1)

;; hignligh current line
(global-hl-line-mode)

;; Nyan mode
;; (nyan-mode)

;; midnight mode
(midnight-mode)
(add-hook 'midnight-hook '(colawithsauce/org-agenda-write-file))

;; White space. COPY FROM kinono
(use-package whitespace
  :ensure nil
  :hook (prog-mode . whitespace-mode)
  :config
  ;; Don't use different background for tabs.
  (face-spec-set 'whitespace-tab
                 '((t :background unspecified)))
  ;; Only use background and underline for long lines, so we can still have
  ;; syntax highlight.

  ;; For some reason use face-defface-spec as spec-type doesn't work.  My guess
  ;; is it's due to the variables with the same name as the faces in
  ;; whitespace.el.  Anyway, we have to manually set some attribute to
  ;; unspecified here.
  (face-spec-set 'whitespace-line
                 '((((background light))
                    :background "#d8d8d8" :foreground unspecified
                    :underline t :weight unspecified)
                   (t
                    :background "#404040" :foreground unspecified
                    :underline t :weight unspecified)))

  ;; Use softer visual cue for space before tabs.
  (face-spec-set 'whitespace-space-before-tab
                 '((((background light))
                    :background "#d8d8d8" :foreground "#de4da1")
                   (t
                    :inherit warning
                    :background "#404040" :foreground "#ee6aa7")))

  (setq
   whitespace-line-column nil
   whitespace-style
   '(face             ; visualize things below:
     empty            ; empty lines at beginning/end of buffer
     lines-tail       ; lines go beyond `fill-column'
     space-before-tab ; spaces before tab
     trailing         ; trailing blanks
     tabs             ; tabs (show by face)
     tab-mark         ; tabs (show by symbol)
     )))

;; Undo and Redo Window changes.
(use-package winner-mode
  :ensure nil
  :hook (after-init . winner-mode))

(use-package ediff
  :ensure nil
  :after winner-mode
  :hook (ediff-quit . winner-undo))

;; unadvice
(defun advice-unadvice (sym)
  "Remove all advices from symbol SYM."
  (interactive "aFunction symbol: ")
  (advice-mapc (lambda (advice _props) (advice-remove sym advice)) sym))

;; highlight diff
(use-package diff-hl
  :config
  (global-diff-hl-mode)
  (diff-hl-flydiff-mode)
  (add-hook 'dired-mode-hook #'diff-hl-dired-mode))

;; Diminish
(diminish 'eldoc-mode)

;; ibuffer grouping settings
(use-package ibuffer-projectile
  :config
  (add-hook 'ibuffer-hook
           (lambda ()
             (ibuffer-projectile-set-filter-groups)
             (unless (eq ibuffer-sorting-mode 'alphabetic)
               (ibuffer-do-sort-by-alphabetic))))
 )
;; Doom modeline is awesome
(doom-modeline-mode)

;; all the icons.
(defun colawithsauce/all-the-icons-enable ()
  (all-the-icons-ibuffer-mode +1)
  (all-the-icons-ivy-rich-mode +1)
  (add-hook 'dired-mode-hook #'all-the-icons-dired-mode)
  (setq doom-modeline-icon t)
  )
(defun colawithsauce/all-the-icons-disable ()
  (all-the-icons-ibuffer-mode -1)
  (all-the-icons-ivy-rich-mode -1)
  (remove-hook 'dired-mode-hook #'all-the-icons-dired-mode)
  (setq doom-modeline-icon nil)
  )
(defun colawithsauce/all-the-icons-smart-toggle (&optional frame)
  "Enable icons when frame displayed at x, else disable it."
  (interactive)
  (if (or (eq (framep frame) 'x)
          (eq (framep (selected-frame)) 'x))
      (colawithsauce/all-the-icons-enable)
    (colawithsauce/all-the-icons-disable)))

(add-hook 'after-make-frame-functions #'colawithsauce/all-the-icons-smart-toggle)
(add-hook 'focus-in-hook #'colawithsauce/all-the-icons-smart-toggle)
(add-hook 'window-buffer-change-functions #'colawithsauce/all-the-icons-smart-toggle)

;; Helpful
(use-package helpful
  :after counsel
  :config
  (setq counsel-describe-function-function #'helpful-callable)
  (setq counsel-describe-variable-function #'helpful-variable)
  )

(defun colawithsauce/toggle-transparency ()
  (interactive)
  (let ((lst default-frame-alist))
      (mapc (lambda (x)
           (when (equal (car x) 'alpha)
             (setq default-frame-alist (remove x default-frame-alist))))
            lst)
      default-frame-alist)
  (let ((alpha (frame-parameter nil 'alpha)))
    (set-frame-parameter
     nil 'alpha
     (if (eql (cond ((numberp alpha) alpha)
                    ((numberp (cdr alpha)) (cdr alpha))
                    ;; Also handle undocumented (<active> <inactive>) form.
                    ((numberp (cadr alpha)) (cadr alpha))
                    ;; Also handle condition that alpha is nil
                    ((not alpha) 100)
                    )
              100)
         `(,+transparency . ,+transparency) '(100 . 100))))
  (add-to-list 'default-frame-alist `(alpha . ,(frame-parameter nil 'alpha))))

(provide 'init-basic)
