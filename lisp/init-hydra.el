;;; init-management.el -*- lexical-binding: t -*-

;;; hydra
;; Basic hydra
(pretty-hydra-define basic-hydra (:title "Basic hydra" :color amaranth :quit-key "q")
  (
   "Toggle"
   (("t b" display-battery-mode "Battery" :toggle t)
    ("t t" display-time-mode "Time" :toggle t)
    ("t a" colawithsauce/toggle-transparency "Transparency" :exit t)
    ("C-l" toggle-input-method "Input Method" :exit t))
   "Org"
   (("o" org-clock-out "Clock out" :exit t)
    ("p" org-pomodoro "Pomodoro" :exit t))
   "Tools"
   (("." helpful-at-point "Search at point" :exit t)
    ("G" magit "Git status" :exit t)
    ("g" counsel-rg "Counsel rg" :exit t)
    ("i" imenu-list-smart-toggle "Imenu" :exit t)
    ("b" bookmark-jump "Bookmark" :exit t)
    ("d" deft "Deft" :exit t))
   "Translate"
   (("y y" define-it-at-point "Search at point" :exit t)
    ("y u" define-word-at-point "Define word" :exit t)
    ("y v" youdao-dictionary-play-voice-at-point "Play voice" :exit t)
    ("y o" youdao-dictionary-search "Search Other window" :exit t)
    ("y c" toggle-company-english-helper "Company english" :exit t)
    ("y i" insert-translated-name-insert "Insert translate" :exit t))
   "Hide-Show"
   (("m m" hs-hide-all "hide all" :exit t)
    ("m r" hs-show-all "show all" :exit t)
    ("m c" hs-hide-block "hide this" :exit t)
    ("m o" hs-show-block "show this" :exit t)
    ("m a" hs-toggle-hiding "toggle hiding" :exit t))
   ))

(global-set-key (kbd "C-l") #'basic-hydra/body)

;; Major mode hydra
(use-package major-mode-hydra
  :ensure t
  :bind
  ("M-SPC" . major-mode-hydra)
  :config
  ;;; Emacs lisp
  (major-mode-hydra-define emacs-lisp-mode (:quit-key "SPC")
    ("Eval"
     (("b" eval-buffer "buffer")
      ("e" eval-defun "defun")
      ("r" eval-region "region"))
     "REPL"
     (("I" ielm "ielm"))
     "Test"
     (("t" ert "prompt")
      ("T" (ert t) "all")
      ("F" (ert :failed) "failed"))
     "Doc"
     (("f" describe-function "function")
      ("v" describe-variable "variable")
      ("i" info-lookup-symbol "info lookup"))))

  ;; hydra
  (major-mode-hydra-define org-mode (:quit-key "SPC")
    ("Clock"
     (("i" org-clock-in "In")
      ("o" org-clock-out "Out")
      ("g" org-clock-goto "Goto")
      ("x" org-clock-in-last "Last")
      ("r" org-clock-report "Report")
      ("u" org-pomodoro "Pomodoro"))
     "Tools"
     (("a" org-agenda "agenda"))
     "Insert"
     (("(" colawithsauce/org-insert-inline-formula "formula")
      ("$" colawithsauce/org-insert-formula "Formular")
      ("s" colawithsauce/org-screenshot "screenshot"))
     "Position"
     (("n" org-next-visible-heading "forward" :exit nil)
      ("p" org-previous-visible-heading "backward" :exit nil)
      ("{" org-mark-ring-push "push" :exit nil)
      ("}" org-mark-ring-goto "pop" :exit nil)
      ("=" counsel-mark-ring "choose"))
     "Dict"
     (("." define-word-at-point "brief")
      ("?" define-it-at-point "elaborate"))))
  )

;; (use-package anki-editor
;;   :init
;;   (setq-default anki-editor-use-math-jax t)
;;   :custom
;;   (anki-editor-break-consecutive-braces-in-latex t)
;;   :config
;;   (pretty-hydra-define anki-editor-hydra
;;     (:title "Anki editor hydra" :quit-key "q")
;;     ("Move"
;;      (("h" left-char "<")
;;      ("l" right-char ">")
;;      ("j" next-line "v")
;;      ("k" previous-line "^"))
;;      "Functions"
;;      (("i" anki-editor-insert-note "Insert note" :exit t)
;;       ("p" anki-editor-push-notes "Push notes" :exit t)
;;       ("e" anki-editor-export-subtree-to-html "Export subtree" :exit t)
;;       ("c" anki-editor-cloze-dwim "Make cloze" :exit t))))
;;   (major-mode-hydra-define+ org-mode nil
;;     ("Tools"
;;      (("e" anki-editor-hydra/body "Anki Editor")))))


;;; Provide feature
(provide 'init-hydra)
