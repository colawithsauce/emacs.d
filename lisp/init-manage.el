;; -*- lexical-binding: t; -*-

(use-package projectile
  :hook (after-init . projectile-mode)
  :bind
  (:map projectile-mode-map
        ("C-c p" . projectile-command-map)))

(provide 'init-manage)
