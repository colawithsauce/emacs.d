;; require package
(require 'init-modular-edit)
(require 'init-basic)  ;Basic
(require 'init-search) ;Searching engine
(require 'init-lang)   ;Language
(require 'init-edit)   ;Totally editing configurations
(require 'init-tools)
(require 'init-extern-packages)

(require 'init-manage)
(require 'init-hydra)

(require 'init-fonts)

;; provide
(provide 'load-all-file)
