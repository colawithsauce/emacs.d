;;; init-lang.el  -*- lexical-binding: t; -*-

;; common settings for each lang
(add-hook 'prog-mode-hook #'hs-minor-mode)
(diminish 'hs-minor-mode)

;; special settings
(add-to-list 'load-path "~/.emacs.d/lisp/lang")
(require 'init-org)                     ;Org-mode
(require 'init-octave)                  ;Octave
(require 'init-lisp)                    ;Lisp
(require 'init-cc)                      ;cc
(require 'init-python)                  ;python
(require 'init-plantuml)


;; provide feature
(provide 'init-lang)
