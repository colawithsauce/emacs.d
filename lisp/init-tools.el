;;  -*- lexical-binding: t; -*-

;; ;; languagetool settings
;; (use-package langtool
;;   :commands langtool-hydra/body
;;   :config
;;   (setq langtool-bin "/usr/bin/languagetool")
;;   (setq langtool-default-language "en-US")
;;   (pretty-hydra-define langtool-hydra (:title "Langtool hydra" :color red :quit-key "q")
;;     (
;;      "Langtool"
;;      (
;;       ("n" langtool-goto-next-error "Next Error")
;;       ("p" langtool-goto-previous-error "Previous Error")
;;       ("o" langtool-check "Check" :exit t)
;;       ("O" langtool-check-done "Done" :exit t)
;;       ("i" langtool-show-message-at-point "Message")
;;       ("c" langtool-correct-buffer "Correct"))
;;      ))
;;   )

(use-package cal-china-x
  :after calendar
  :config
  (setq mark-holidays-in-calendar t)
  (setq cal-china-x-important-holidays cal-china-x-chinese-holidays)
  (setq cal-china-x-general-holidays '((holiday-lunar 1 15 "元宵节")))
  (setq calendar-holidays
        (append cal-china-x-important-holidays
                cal-china-x-general-holidays)))


(use-package google-translate
  :config
  (defun google-translate--search-tkk () "Search TKK." (list 430675 2721866130))
  (setq google-translate-default-source-language "auto")
  (setq google-translate-default-target-language "en"))

(use-package define-it
  :after google-translate
  :custom
  (define-it-show-wiki-summary nil)
  (define-it-show-google-translate nil)
  (define-it-output-choice 'view))

;; Novel reading
(use-package nov
  :custom
  (nov-text-width t)
  :hook
  (nov-mode . visual-line-mode)
  :mode
  (("\\.epub\\'" . nov-mode)))

(use-package hl-todo
  :hook (after-init . hl-todo-mode))

;; Search Notes
(use-package deft
    :bind
    (("C-x C-d" . deft))
    :config
    (setq deft-extensions '("org"))
    (setq deft-directory "~/Notes/")
    (setq deft-recursive t)
    (setq deft-file-naming-rules '((noslash . "_")))
    (setq deft-text-mode 'org-mode)
    (setq deft-use-filter-string-for-filename t)
    (setq deft-org-mode-title-prefix t)
    (setq deft-use-filename-as-title nil)
    (setq deft-strip-summary-regexp
          (concat "\\("
                  "[\n\t]" ;; blank
                  "\\|^#\\+[[:upper:]_]+:.*$" ;; org-mode metadata
                  "\\|^#\\+[[:alnum:]_]+:.*$" ;; org-mode metadata
                  "\\)")))

(with-eval-after-load 'imenu
  (defun colawithsauce/elisp-imenu-extend ()
  (setq imenu-generic-expression
        `(("Section" "^[ \t]*;;;;*[ \t]+\\([^\n]+\\)" 1)
          ("Evil commands" "^\\s-*(evil-define-\\(?:command\\|operator\\|motion\\) +\\(\\_<[^ ()\n]+\\_>\\)" 1)
          ("Unit tests" "^\\s-*(\\(?:ert-deftest\\|describe\\) +\"\\([^\")]+\\)\"" 1)
          ("Package" "^\\s-*(\\(?:;;;###package\\|package!\\|use-package!?\\|after!\\) +\\(\\_<[^ ()\n]+\\_>\\)" 1)
          ("Major modes" "^\\s-*(define-derived-mode +\\([^ ()\n]+\\)" 1)
          ("Minor modes" "^\\s-*(define-\\(?:global\\(?:ized\\)?-minor\\|generic\\|minor\\)-mode +\\([^ ()\n]+\\)" 1)
          ("Modelines" "^\\s-*(def-modeline! +\\([^ ()\n]+\\)" 1)
          ("Modeline segments" "^\\s-*(def-modeline-segment! +\\([^ ()\n]+\\)" 1)
          ("Advice" "^\\s-*(\\(?:def\\(?:\\(?:ine-\\)?advice!?\\)\\) +\\([^ )\n]+\\)" 1)
          ("Macros" "^\\s-*(\\(?:cl-\\)?def\\(?:ine-compile-macro\\|macro\\) +\\([^ )\n]+\\)" 1)
          ("Inline functions" "\\s-*(\\(?:cl-\\)?defsubst +\\([^ )\n]+\\)" 1)
          ("Functions" "^\\s-*(\\(?:cl-\\)?def\\(?:un\\|un\\*\\|method\\|generic\\|-memoized!\\) +\\([^ ,)\n]+\\)" 1)
          ("Variables" "^\\s-*(\\(def\\(?:c\\(?:onst\\(?:ant\\)?\\|ustom\\)\\|ine-symbol-macro\\|parameter\\|var\\(?:-local\\)?\\)\\)\\s-+\\(\\(?:\\sw\\|\\s_\\|\\\\.\\)+\\)" 2)
          ("Types" "^\\s-*(\\(cl-def\\(?:struct\\|type\\)\\|def\\(?:class\\|face\\|group\\|ine-\\(?:condition\\|error\\|widget\\)\\|package\\|struct\\|t\\(?:\\(?:hem\\|yp\\)e\\)\\)\\)\\s-+'?\\(\\(?:\\sw\\|\\s_\\|\\\\.\\)+\\)" 2))))
  (add-hook 'emacs-lisp-mode-hook 'colawithsauce/elisp-imenu-extend))


(provide 'init-tools)
