;;; init-edit.el  -*- lexical-binding: t; -*-

;;; Built-in functions

;; Revert the file if changed in disk
(global-auto-revert-mode)

;; pair
(electric-pair-mode)

;; delete selection mode
(delete-selection-mode)

;; ispell integral
(setq-default ispell-choices-win-default-height 4)

;;; Packages setting

;; Pinyin jump
(use-package ace-pinyin
  :delight
  :config
  (ace-pinyin-global-mode)
  (require 'my-custom-pinyinlib))

;; zap
(use-package avy-zap
  :delight
  :bind
  (("M-z" . 'avy-zap-to-char-dwim)))

;; Replace indicator
(use-package anzu
  :delight
  :hook (after-init . global-anzu-mode)
  :config
  (global-set-key [remap query-replace] 'anzu-query-replace)
  (global-set-key [remap query-replace-regexp] 'anzu-query-replace-regexp))

;; Complete
(use-package company
  :delight
  :bind
  (:map company-active-map
        ("C-n" . company-select-next)
        ("C-p" . company-select-previous))
  :config
  (global-company-mode)
  (setq company-global-modes '(not org-mode)))

(use-package company-posframe
  :delight
  :custom
  (company-posframe-font (face-attribute 'default :fontset))
  :after company
  :hook (company-mode . company-posframe-mode))

;; Input method
(use-package rime
  :custom
  (default-input-method "rime")
  (rime-disable-predicates
   '(rime-predicate-org-latex-mode-p
     rime-predicate-hydra-p
     rime-predicate-org-in-src-block-p
     rime-predicate-ace-window-p
     meow-normal-mode-p
     meow-keypad-mode-p)))

;; Split English and Chinese with Space
(use-package pangu-spacing
  :config
  (global-pangu-spacing-mode 1)
  (setq pangu-spacing-real-insert-separtor t))

(provide 'init-edit)
