;;; /home/wzn/.emacs.d/lisp/lang/init-lisp.el  -*- lexical-binding: t; -*- 


(add-hook 'emacs-lisp-mode-hook #'paredit-mode)
(diminish 'paredit-mode)

;;; provide feature
(provide 'init-lisp)
