;; -*- lexical-binding: t; -*-

(use-package plantuml-mode
  :custom
  ;; Sample executable configuration
  (plantuml-jar-path "~/.emacs.d/tools/plantuml.jar")
  (plantuml-default-exec-mode 'jar)
  :config
  (with-eval-after-load 'org
    (setq org-plantuml-exec-mode plantuml-default-exec-mode)
    (setq org-plantuml-jar-path plantuml-jar-path))
  )
(provide 'init-plantuml)
