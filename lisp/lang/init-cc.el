;; -*- lexical-binding: t; -*-

;;; lsp settings.
(use-package lsp-mode
    :hook (;; replace XXX-mode with concrete major-mode(e. g. python-mode)
           (c-mode-common . lsp)
           ;; if you want which-key integration
           (lsp-mode . lsp-enable-which-key-integration)
           (lsp-mode . lsp-enable-imenu))
    :commands lsp
    :custom
    ((lsp-prefer-flymake nil)
     (lsp-auto-guess-root t))
    :init
    (setq lsp-keymap-prefix "C-c l")
    :config
    (require 'dap-cpptools))

(use-package yasnippet
  :delight (yas-minor-mode " Yas")
  :config
  (yas-global-mode))

(use-package flycheck
  :delight
  :config
  (flymake-mode-off))

(use-package lsp-treemacs :commands lsp-treemacs-errors-list)

(use-package lsp-ui :commands lsp-ui
  :custom
  (lsp-ui-doc-enable . nil))

;; (use-package dap-mode
;;   :config
;;   (require 'dap-gdb-lldb))

;; (use-package ccls
;;   :hook ((c-mode c++-mode objc-mode cuda-mode) .
;;          (lambda () (require 'ccls) (lsp)))
;;   :custom
;;   (ccls-executable "/usr/bin/ccls")
;;   (ccls-initialization-options
;;    '(:index (:comments 2 :threads 2 :onChange t)
;;      :completion (:detailedLabel t)
;;      :clang (:resourceDir "/usr/lib/clang/11.0.0")
;;      :cache (:directory "/tmp/ccls-cache")
;;      )))

(use-package modern-cpp-font-lock
  :hook (c-mode-common . modern-c++-font-lock-mode)
  :diminish)

;; (use-package ggtags
;;   :hook (c-mode-common . ggtags-mode))

;; (use-package counsel-gtags
;;   :hook (c-mode-common . counsel-gtags-mode))

;; ;;; irony setting

;; (use-package irony
;;   :after (flycheck company)
;;   :hook
;;   (c-mode-common . irony-mode)
;;   (irony-mode . irony-cdb-autosetup-compile-options)
;;   (irony-mode . irony-eldoc)
;;   (irony-mode . flycheck-irony-setup)
;;   )

;; ;; Load with `irony-mode` as a grouped backend
;; (with-eval-after-load 'company
;;   '(add-to-list
;;     'company-backends '(company-irony-c-headers company-irony)))

(require 'google-c-style)

(defun colawithsauce/c-set-style ()
  "Setting C-Style of file."
  (c-add-style "microsoft"
               '("stroustrup"
                 (c-basic-offset . 2)
                 (c-offsets-alist
                  (innamespace . -)
                  (inline-open . 0)
                  (inher-cont . c-lineup-multi-inher)
                  (arglist-cont-nonempty . +)
                  (template-args-cont . +))))
  (c-set-style "microsoft")
  )
(defun colawithsauce/lsp-save-cc ()
  (add-hook 'before-save-hook #'lsp-format-buffer t t)
  (add-hook 'before-save-hook #'lsp-organize-imports t t))

(add-hook 'c-mode-common-hook #'display-line-numbers-mode)
(add-hook 'c-mode-common-hook #'google-set-c-style)
(add-hook 'c-mode-common-hook #'colawithsauce/lsp-save-cc)

(provide 'init-cc)
