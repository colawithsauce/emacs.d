;;; Initialize melpa.
(require 'package)
;; (setq package-archives '(("gnu"   . "http://mirrors.tuna.tsinghua.edu.cn/elpa/gnu/")
;;                          ("melpa" . "http://mirrors.tuna.tsinghua.edu.cn/elpa/melpa/")))

(package-initialize)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/") t)

(unless (package-installed-p 'package+)
  (package-install 'package+))

;; Define all package to install
(package-manifest
 ;;; Package management
 'package+
 'use-package

 ;;; Basic config
 'better-defaults
 'undo-tree
 'diminish
 'delight

 ;; modular edit
 'meow

 ;; UI
 'all-the-icons
 'all-the-icons-dired
 'all-the-icons-ibuffer
 'all-the-icons-ivy-rich
 'ivy-rich
 'doom-themes
 'posframe
 'helpful
 'prescient
 'ivy-prescient
 'ibuffer
 'ibuffer-projectile
 'doom-modeline
 'diff-hl
 'nyan-mode
 'simple-modeline

 ;;; Searching
 'ivy
 'counsel
 'swiper
 'which-key
 'amx

 ;;; Edit integation
 'avy
 'avy-zap
 'ace-pinyin
 'anzu
 'company
 'company-posframe
 'company-prescient
 'rime
 'imenu-list
 'yasnippet
 'yasnippet-snippets
 'writeroom-mode
 'flycheck
 'sudo-edit
 'yaml-mode
 'flyspell-correct-ivy
 'wgrep
 'pangu-spacing

 ;;; Tools
 'youdao-dictionary
 'langtool
 'org-drill
 'define-word
 'define-it
 'nov
 'ssh-deploy
 'nginx-mode
 'company-nginx
 'crontab-mode
 'hl-todo
 'pdf-tools
 'cal-china-x
 'restart-emacs
 'deft
 'anki-connect
 'anki-editor

;;; Languages
 'lsp-mode
 'lsp-treemacs
 'lsp-ui
 'dap-mode

 ;; Org-mode
 'org
 'org-superstar
 'org-download
 'org2ctex
 'org-roam
 'org-roam-server
 'ox-hugo
 ;; 'org-super-agenda
 'idle-org-agenda
 'elegant-agenda-mode
 'valign
 'org-pomodoro

 ;; emacs-lisp mode
 'paredit

 ;; cc
 'cmake-mode
 'ccls
 'google-c-style
 'modern-cpp-font-lock
 'ggtags
 'counsel-gtags

 'irony
 'irony-eldoc
 'company-irony
 'flycheck-irony
 'company-irony-c-headers

 ;; latex
 'cdlatex
 'auctex

 ;; js
 'js2-mode
 'js-format
 'js-comint

 ;; plantuml
 'ob-napkin
 'plantuml-mode

 ;; vue.js
 'vue-mode

 ;; typescript
 'typescript-mode
 'ob-typescript
 'ts-comint

 ;; python
 'lsp-pyright
 'py-autopep8

 ;;; Manage
 'pretty-hydra
 'use-package-hydra
 'major-mode-hydra

 ;; Project manage
 'magit
 'projectile
 'treemacs-projectile
 'benchmark-init
 )

(cl-letf (((symbol-function 'define-obsolete-function-alias) #'defalias))
  (use-package benchmark-init
    :config
    (require 'benchmark-init-modes)  ; explicitly required
    (add-hook 'after-init-hook #'benchmark-init/deactivate)))

;; Add load-path
(dolist (dir '("lisp" "site-lisp"))
  (push (expand-file-name dir user-emacs-directory) load-path))

;; Add subdirs
(let ((default-directory (expand-file-name "site-lisp" user-emacs-directory)))
  (normal-top-level-add-subdirs-to-load-path))

;; Single custom file
(setq custom-file "~/.emacs.d/custom.el")
(when (file-exists-p custom-file)
  (load custom-file))

;; set exec-path
(let ((path (shell-command-to-string ". ~/.zshrc > /dev/null; echo -n $PATH")))
  (setenv "PATH" path)
  (setq exec-path
        (append
         (split-string-and-unquote path ":")
         exec-path)))

;; load
(require 'load-all-file)
(put 'narrow-to-region 'disabled nil)
